//
//  VueTangleKit.js
//  A port of TangleKit using Vue.js components
//
//  Created by Bret Victor on 6/10/11.
//  Port by Pierre de La Morinerie on 11/06/2018.
//  (c) 2011 Bret Victor.  MIT open-source license.
//

//----------------------------------------------------------
//
//  tk-adjustable-number
//
//  Drag a number to adjust.
//
//  Attributes:  min (optional): minimum value
//               max (optional): maximum value
//               step (optional): granularity of adjustment (can be fractional)

Vue.component('tk-adjustable-number', {
  props: {
    value: { type: Number, required: true },
    min:   { type: Number, default: 1 },
    max:   { type: Number, default: 10 },
    step:  { type: Number, default: 1 }
  },

  data: function () {
    return {
      valueAtMouseDown: 0,
      clientXAtMouseDown: 0,
      isHovering: false,
      isDragging: false,
      mouseBound: {
        touchDidMoveHandler: null,
        touchDidGoUpHandler: null
      }
    }
  },

  computed: {
    isActive: function() {
      return this.isDragging || this.isHovering;
    },
    hasHoverClass: function() {
      return !this.isDragging && this.isActive;
    }
  },

  methods: {
    clamp: function(number, min, max) {
      return Math.min(Math.max(number, min), max);
    },

    onMouseEnter: function() {
      this.isHovering = true;
      this.updateRolloverEffects();
    },

    onMouseLeave: function() {
      this.isHovering = false;
      this.updateRolloverEffects();
    },

    updateRolloverEffects: function () {
      this.updateCursor();
      this.updateHelp();
    },

    updateCursor: function () {
      let body = document.body;
      if (this.isActive) { body.classList.add("TKCursorDragHorizontal"); }
      else { body.classList.remove("TKCursorDragHorizontal"); }
    },

    // Help
    updateHelp: function () {
      let size = this.$refs.root.getBoundingClientRect(),
          top = -size.height + 7;
          left = Math.round(0.5 * (size.width - 20));
          display = this.isHovering ? "block" : "none";

      let helpElement = this.$refs.help;
      helpElement.style.left = left + 'px';
      helpElement.style.top = top + 'px';
      helpElement.style.display = display;
    },

    // Drag
    touchDidGoDown: function(mouseEvent) {
      this.valueAtMouseDown = this.value;
      this.clientXAtMouseDown = mouseEvent.clientX;
      this.isDragging = true;
      this.updateRolloverEffects();
      // Register mouse move events
      this.mouseBound = {
        touchDidMoveHandler: this.touchDidMove.bind(this),
        touchDidGoUpHandler: this.touchDidGoUp.bind(this)
      }
      document.addEventListener('mousemove', this.mouseBound.touchDidMoveHandler);
      document.addEventListener('mouseup',   this.mouseBound.touchDidGoUpHandler);
      // Prevent the text from being selected
      mouseEvent.preventDefault();
    },

    touchDidMove: function(mouseEvent) {
      let offsetX = mouseEvent.clientX - this.clientXAtMouseDown,
      value = this.valueAtMouseDown + offsetX / 5 * this.step;
      newValue = Math.round(value / this.step) * this.step;
      this.$emit('input', this.clamp(newValue, this.min, this.max));
      this.updateHelp();

      mouseEvent.preventDefault();
    },

    touchDidGoUp: function(mouseEvent) {
      this.isDragging = false;
      if (this.mouseBound) {
        document.removeEventListener('mousemove', this.mouseBound.touchDidMoveHandler);
        document.removeEventListener('mouseup',   this.mouseBound.touchDidGoUpHandler);
        this.mouseBound = null;
      }
      this.updateRolloverEffects();
      this.$refs.help.style.display = 'none';
    }
  },

  template: '<span ref="root" \
                   class="TKAdjustableNumber" \
                   v-bind:class="{ TKAdjustableNumberDown: isDragging, TKAdjustableNumberHover: hasHoverClass }" \
                   v-on:mouseenter="onMouseEnter" \
                   v-on:mouseleave="onMouseLeave" \
                   v-on:mousedown="touchDidGoDown"> \
                <div ref="help" class="TKAdjustableNumberHelp" style="display: none">drag</div> \
                {{value}}<slot></slot> \
             </span>'
});
